import AppStroage from "./AppStroage"
import Token from "./Token"

class User {
    responseAfterLogin(res) {
        const access_token = res.data.access_token
        const username = res.data.name
        console.log(username)

        console.log(access_token)
        if (Token.isValid(access_token)) {
            AppStroage.store(access_token, username);
        }
    }

    hasToken() {
        const storeToken = localStorage.getItem("token");
        if (storeToken) {
            return Token.isValid(storeToken) ? true : false;
        }
        return false;
    }

    logedIn() {
        return this.hasToken();
    }

    logedOut() {
        AppStroage.clear();
        window.location = '/';
    }

    name() {
        if (this.logedIn) {
            return localStorage.getItem("user");
        }
    }

    id() {
        if (this.logedIn) {
            const payload = Token.payload(localStorage.getItem("token"));
            return payload.sub;
        }
    }
}
export default User = new User();
