
let login = require('./components/auth/login.vue').default;
let reg = require('./components/auth/register.vue').default;
let forget = require('./components/auth/forget.vue').default;
let home = require('./components/home.vue').default;
let logout = require('./components/auth/logout.vue').default;


export const routes = [
    { path: '/', component: login , name: '/' },
    { path: '/register', component: reg , name: 'register' },
    { path: '/forget', component: forget , name: 'forget_password' },
    { path: '/home', component: home , name: 'home' },
    { path: '/logout', component: logout , name: 'logout' },
  ]
